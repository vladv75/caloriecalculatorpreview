package de.android.caloriecalculator.adapter;

/*
 * ClientCardRecyclerAdapter.java    v.1.0 09.03.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.akashandroid90.imageletter.MaterialLetterIcon;

import java.util.List;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.android.caloriecalculator.R;
import de.android.caloriecalculator.dto.ClientCard;

public class ClientCardRecyclerAdapter
        extends RecyclerView.Adapter<ClientCardRecyclerAdapter.ViewHolder> {
    private Context context;
    private static final Random RANDOM = new Random();
    private final TypedValue mTypedValue = new TypedValue();
    private int mBackground;
    private List<ClientCard> clientCards;
    private int[] mMaterialColors;

    public ClientCardRecyclerAdapter(Context context, List<ClientCard> items) {
        this.context = context;
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mMaterialColors = context.getResources().getIntArray(R.array.colors);
        mBackground = mTypedValue.resourceId;
        clientCards = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        view.setBackgroundResource(mBackground);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        String lastFirstName = clientCards.get(position).getFirstName() + " " + clientCards.get(position).getLastName();
        int randomColor = mMaterialColors[RANDOM.nextInt(mMaterialColors.length)];

        holder.mIcon.setInitials(true);
        holder.mIcon.setInitialsNumber(2);
        holder.mIcon.setLetterSize(24);
        holder.mIcon.setLetter(lastFirstName);
        holder.mIcon.setShapeType(MaterialLetterIcon.SHAPE_CIRCLE);
        holder.mIcon.setShapeColor(randomColor);

        holder.textViewFIO.setText(lastFirstName);
        holder.textViewFIO.setTextColor(randomColor);
        holder.textViewMail.setText(clientCards.get(position).getEMail());
        holder.textViewPhone.setText(clientCards.get(position).getTelNum());
        holder.textViewParam.setText(String.format(
                        "%s: %.1f " +
                        "%s: %.1f " +
                        "%s: %.1f " +
                        "%s: %.1f " +
                        "%s: %.2f " +
                        "%s: %.2f " +
                        "%s: %.2f",
                context.getString(R.string.imt), clientCards.get(position).getDataIMT(),
                context.getString(R.string.BM), clientCards.get(position).getDataBM(),
                context.getString(R.string.weight_lower_bound), clientCards.get(position).getWeightDown(),
                context.getString(R.string.weight_upper_bound), clientCards.get(position).getWeightUp(),
                context.getString(R.string.BV), clientCards.get(position).getDataOO(),
                context.getString(R.string.MSG_lower_bound), clientCards.get(position).getDataMSGdown(),
                context.getString(R.string.MSG_upper_bound), clientCards.get(position).getDataMSGup())
        );
    }

    @Override
    public int getItemCount() {
        return clientCards.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.icon) MaterialLetterIcon mIcon;
        @Bind(R.id.textFIO) TextView textViewFIO;
        @Bind(R.id.textMail) TextView textViewMail;
        @Bind(R.id.textPhone) TextView textViewPhone;
        @Bind(R.id.textViewParam) TextView textViewParam;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}