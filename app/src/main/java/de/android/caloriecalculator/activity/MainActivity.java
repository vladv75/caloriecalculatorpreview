package de.android.caloriecalculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import de.android.caloriecalculator.R;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.fragment.AboutUsFragment;
import de.android.caloriecalculator.fragment.BlankFragment;
import de.android.caloriecalculator.fragment.ClientsFragment;
import de.android.caloriecalculator.fragment.FeedbackFragment;
import de.android.caloriecalculator.util.Utils;

/*
 * MainActivity.java    v.1.2 01.07.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ClientCard managerCard = Utils.getCurrentManagerCard(this);
        View header = navigationView.getHeaderView(0);
        TextView tv_name = (TextView) header.findViewById(R.id.tv_name_nh);
        tv_name.setText(String.format("%s %s", managerCard.getFirstName(), managerCard.getLastName()));
        TextView tv_mail = (TextView) header.findViewById(R.id.tv_mail_nh);
        tv_mail.setText(managerCard.getEMail());
        openFragment("ClientsFragment");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.menu_profile) {
            startActivity(new Intent(MainActivity.this, UserProfileActivity.class));
        } else if (id == R.id.menu_clients) {
            openFragment("ClientsFragment");
        } else if (id == R.id.menu_feedback) {
            openFragment("FeedbackFragment");
        } else if (id == R.id.menu_about) {
            openFragment("AboutUsFragment");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openFragment(String fragmentName) {
        Fragment fragment = new BlankFragment();
        switch (fragmentName) {
            case "FeedbackFragment":
                fragment = new FeedbackFragment();
                break;
            case "BlankFragment":
                fragment = new BlankFragment();
                break;
            case "ClientsFragment":
                fragment = new ClientsFragment();
                break;
            case "AboutUsFragment":
                fragment = new AboutUsFragment();
                break;
        }
        FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
    }
}
