package de.android.caloriecalculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.android.caloriecalculator.R;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.fragment.UserProfile.ProfileUserFragment;
import de.android.caloriecalculator.fragment.UserProfile.RecomendationUserFragment;
import de.android.caloriecalculator.fragment.UserProfile.ResultUserFragment;
import de.android.caloriecalculator.util.Utils;

/*
 * UserProfileActivity.java    v.1.1 30.06.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class UserProfileActivity extends AppCompatActivity {

    private ClientCard currentManagerCard;

    @Bind(R.id.tvFIO)       TextView tvFIO;
    @Bind(R.id.tvEmail)     TextView tvEmail;
    @Bind(R.id.tvTelephone) TextView tvTelephone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userprofile);
        ButterKnife.bind(this);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.userprofile_tabs);
        ViewPager viewPager  = (ViewPager) findViewById(R.id.userprofile_viewpager);

        Toolbar toolbar = (Toolbar) findViewById(R.id.userprofile_toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                onBackPressed();
            }
        });

        toolbar.inflateMenu(R.menu.userprofile_menuitem);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(getApplicationContext(), EditClientProfileActivity.class);
                intent.putExtra("sourceActivity", "UserProfileActivity");
                startActivity(intent);
                return true;
            }
        });

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        load();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
    }

    private void setupViewPager(ViewPager viewPager) {
        TabsAdapter adapter = new TabsAdapter(getSupportFragmentManager());
        adapter.addFragment(ProfileUserFragment.newInstance(),
                getResources().getString(R.string.tab_name_profile));
        adapter.addFragment(ResultUserFragment.newInstance(),
                getResources().getString(R.string.tab_name_result));
        adapter.addFragment(RecomendationUserFragment.newInstance(),
                getResources().getString(R.string.tab_name_recomendation));
        viewPager.setAdapter(adapter);
    }

    class TabsAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public Fragment getItem(int i) {
            return fragmentList.get(i);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }
    }

    public ClientCard takeManagerCard() {
        return currentManagerCard;
    }

    private void load() {
        currentManagerCard = Utils.getCurrentManagerCard(UserProfileActivity.this);
        tvFIO.setText(String.format("%s %s", currentManagerCard.getFirstName(), currentManagerCard.getLastName()));
        tvEmail.setText(currentManagerCard.getEMail());
        tvTelephone.setText(currentManagerCard.getTelNum());
    }

}
