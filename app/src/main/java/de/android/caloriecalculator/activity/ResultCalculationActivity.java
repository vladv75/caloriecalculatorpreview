package de.android.caloriecalculator.activity;

/*
 * ResultCalculationActivity.java    v.1.0 09.03.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.creativityapps.gmailbackgroundlibrary.BackgroundMail;

import java.util.ArrayList;
import java.util.List;

import de.android.caloriecalculator.R;
import butterknife.Bind;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.fragment.ResultCalculation.ProfileFragment;
import de.android.caloriecalculator.fragment.ResultCalculation.RecomendationFragment;
import de.android.caloriecalculator.fragment.ResultCalculation.ResultFragment;
import de.android.caloriecalculator.service.ClientDAO;
import de.android.caloriecalculator.util.PdfUtil;

public class ResultCalculationActivity extends AppCompatActivity {
    ClientCard clientCard;

    @Bind(R.id.tvFIO) TextView tvFIO;
    @Bind(R.id.tvEmail) TextView tvEmail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultcalculation);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.resultcalculation_tabs);
        ViewPager viewPager  = (ViewPager) findViewById(R.id.resultcalculation_viewpager);

        Toolbar toolbar = (Toolbar) findViewById(R.id.resultcalculation_toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolbar.inflateMenu(R.menu.resultcalculation_menuitem);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(getApplicationContext(), EditClientProfileActivity.class);
                long id = clientCard.getId();
                intent.putExtra("id", id);
                intent.putExtra("sourceActivity", "ResultCalculationActivity");
                startActivity(intent);
                return true;
            }
        });

        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendReport2Email();
            }
        });

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        setupResultData();
    }

    private void sendReport2Email() {
        String filePath = PdfUtil.createPdfFromClient(this, clientCard);
        BackgroundMail.newBuilder(this)
                .withUsername("nl.calorie.calculator@gmail.com")
                .withPassword("nl123456")
                .withMailto(clientCard.getEMail())
                .withSubject("Calorie Calculator Client Report")
                .withBody("Отчет по клиенту: " + clientCard.getFirstName() + " " + clientCard.getLastName())
                .withOnSuccessCallback(new BackgroundMail.OnSuccessCallback() {
                    @Override
                    public void onSuccess() {
                        //Toast.makeText(ResultCalculationActivity.this, R.string.success, Toast.LENGTH_SHORT).show();
                    }
                })
                .withOnFailCallback(new BackgroundMail.OnFailCallback() {
                    @Override
                    public void onFail() {
                        //Toast.makeText(ResultCalculationActivity.this, R.string.failed, Toast.LENGTH_SHORT).show();
                    }
                })
                .withAttachments(filePath)
                .withProcessVisibility(true)
                .send();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class).
                setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    private void setupViewPager(ViewPager viewPager) {
        TabsAdapter adapter = new TabsAdapter(getSupportFragmentManager());
        adapter.addFragment(ResultFragment.newInstance(),
                getResources().getString(R.string.tab_name_result));
        adapter.addFragment(RecomendationFragment.newInstance(),
                getResources().getString(R.string.tab_name_recomendation));
        adapter.addFragment(ProfileFragment.newInstance(),
                getResources().getString(R.string.tab_name_profile));
        viewPager.setAdapter(adapter);
    }

    class TabsAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public Fragment getItem(int i) {
            return fragmentList.get(i);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }
    }

    public ClientCard takeClientCard() {
        return clientCard;
    }

    private void setupResultData() {
        ClientDAO clientDAO = new ClientDAO(this);
        Intent intent = getIntent();
        long id = intent.getLongExtra("id", 0);
        clientCard = clientDAO.findById(id);

        tvFIO = (TextView) findViewById(R.id.tvFIO);
        tvFIO.setText(String.format("%s %s", clientCard.getFirstName(), clientCard.getLastName()));
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        //tvEmail.setText(clientCard.getEMail());
        tvEmail.setText("клиент");
    }
}
