package de.android.caloriecalculator.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

import de.android.caloriecalculator.R;
import de.android.caloriecalculator.util.Utils;

public class IntroActivity extends AppIntro {
    @Override
    public void init(Bundle savedInstanceState) {
        addSlide(AppIntroFragment.newInstance(
                getString(R.string.monitor_your_health),
                getString(R.string.join_us),
                R.drawable.nl_2,
                Color.parseColor("#004D40")));
        addSlide(AppIntroFragment.newInstance(
                getString(R.string.the_unique_methodology),
                getString(R.string.three_simple_steps),
                R.drawable.nl_3,
                Color.parseColor("#E65100")));
        addSlide(AppIntroFragment.newInstance(
                getString(R.string.will_make_the_app_better_together),
                getString(R.string.here_you_can_write_a_responce),
                R.drawable.feedb,
                Color.parseColor("#2d72d9")));
        addSlide(AppIntroFragment.newInstance(
                getString(R.string.let_s_start),
                getString(R.string.fill_out_the_form_and_join_us),
                R.drawable.checkmark,
                Color.parseColor("#37474F")));
        showSkipButton(false);
        setProgressButtonEnabled(true);
        setVibrate(true);
        setVibrateIntensity(30);
        setFlowAnimation();
    }
    @Override
    public void onSkipPressed() {
    }

    @Override
    public void onNextPressed() {
    }

    @Override
    public void onDonePressed() {
        Utils.setInfoTourShown(IntroActivity.this, true);
        startActivity(new Intent(this, RegistrationActivity.class).
                setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    @Override
    public void onSlideChanged() {
    }

}
