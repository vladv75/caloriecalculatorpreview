package de.android.caloriecalculator.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import de.android.caloriecalculator.R;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.fragment.EditClientProfile.EditClientProfileFirstFragment;
import de.android.caloriecalculator.service.ClientDAO;
import de.android.caloriecalculator.util.Utils;

/*
 * EditClientProfileActivity.java    v.1.1 30.06.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class EditClientProfileActivity extends AppCompatActivity {

    private static final int FRAGMENT_FIRST = 1;
    public static final int FRAGMENT_SECOND = 2;
    public int fragmentType;
    ClientCard clientCard;
    ClientDAO clientDAO;
    public TextView tvFIO;
    public TextView tvEmail;
    public String sourceActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editclientprofile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.editclientprofile_toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setupData();
        openFragmentFirst();
    }

    @Override
    public void onBackPressed() {
        switch (fragmentType) {
            case FRAGMENT_FIRST:
                Intent intent;
                switch(sourceActivity) {
                    case "MainActivity":
                        intent = new Intent(this, MainActivity.class);
                        startActivity(intent);
                        break;
                    case "ResultCalculationActivity":
                        intent = new Intent(this, ResultCalculationActivity.class);
                        long id = clientCard.getId();
                        intent.putExtra("id", id);
                        startActivity(intent);
                        break;
                    case "UserProfileActivity":
                        intent = new Intent(this, UserProfileActivity.class);
                        startActivity(intent);
                        break;
                }
                break;
            case FRAGMENT_SECOND:
                super.onBackPressed();
                fragmentType = FRAGMENT_FIRST;
                break;
        }
    }

    private void openFragmentFirst() {
        EditClientProfileFirstFragment fragmentEditclientprofile = new EditClientProfileFirstFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragmentEditclientprofile);
        fragmentTransaction.addToBackStack("EditClientProfileFirstFragment");
        fragmentType = FRAGMENT_FIRST;
        fragmentTransaction.commit();
    }

    public ClientCard takeClientCard() {
        return clientCard;
    }

    public void putClientCard(ClientCard clientCard) {
        this.clientCard = clientCard;
    }

    private void setupData() {
        Intent intent = getIntent();
        long id = intent.getLongExtra("id", 0);
        sourceActivity = intent.getStringExtra("sourceActivity");

        switch(sourceActivity) {
            case "MainActivity":
                clientCard = new ClientCard(this);
                clientDAO = new ClientDAO(this);
                tvFIO = (TextView) findViewById(R.id.tvFIO);
                tvFIO.setText(getResources().getText(R.string.new_card));
                tvEmail = (TextView) findViewById(R.id.tvEmail);
                break;
            case "ResultCalculationActivity":
                clientDAO = new ClientDAO(this);
                clientCard = clientDAO.findById(id);
                tvFIO = (TextView) findViewById(R.id.tvFIO);
                tvFIO.setText(String.format("%s %s", clientCard.getFirstName(), clientCard.getLastName()));
                tvEmail = (TextView) findViewById(R.id.tvEmail);
                tvEmail.setText(clientCard.getEMail());
                break;
            case "UserProfileActivity":
                clientCard = Utils.getCurrentManagerCard(this);
                tvFIO = (TextView) findViewById(R.id.tvFIO);
                tvFIO.setText(String.format("%s %s", clientCard.getFirstName(), clientCard.getLastName()));
                tvEmail = (TextView) findViewById(R.id.tvEmail);
                tvEmail.setText(clientCard.getEMail());
                break;
        }
    }
}
