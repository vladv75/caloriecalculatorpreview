package de.android.caloriecalculator.dto;

/*
 * IClientCard.java    v.1.0 23.03.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public interface IClientCard {
    //gender
    int GENDER_MALE = 0; //"муж";
    int GENDER_FEMALE = 1; //"жен";
    //activityСoeff
    float ACTIVITY_LOW = (float) 1.2;
    float ACTIVITY_SMALL = (float) 1.38;
    float ACTIVITY_MIDDLE = (float) 1.55;
    float ACTIVITY_HIGH = (float) 1.73;
    //somatoType
    int SOMATO_TYPE_FRAIL = 0;
    int SOMATO_TYPE_MEDIUM = 1;
    int SOMATO_TYPE_LARGE = 2;
    //clasIMT
    int LARGE_UNDER_WEIGHT = 0;
    int MILD_UNDER_WEIGHT = 1;
    int SMALL_UNDER_WEIGHT = 2;
    int UNDER_WEIGHT = 3;
    int NORMAL_WEIGHT = 4;
    int OVER_WEIGHT = 5;
    int MILD_OVER_WEIGHT = 6;
    int LARGE_OVER_WEIGHT = 7;
}
