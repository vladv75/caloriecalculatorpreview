package de.android.caloriecalculator.dto;

/*
 * ClientCard.java    v.1.0 09.03.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

import android.content.Context;

import de.android.caloriecalculator.R;

public class ClientCard implements IClientCard {
    private long id;
    private String firstName;
    private String lastName;
    private int gender;
    private String eMail;
    private String telNum;
    private int indexSolov;
    private int height;
    private int weight;
    private int age;
    private int waistSize;
    private float activityСoeff;
    private float dataIMT;
    private float weightDown;
    private float weightUp;
    private float dataBM;
    private float dataOO;
    private float dataMSGdown;
    private float dataMSGup;
    private float proteinsNormCal;
    private float proteinsNormGram;
    private float fatsNormCal;
    private float fatsNormGram;
    private float carbsNormCal;
    private float carbsNormGram;
    private float proteinsDownCal;
    private float proteinsDownGram;
    private float fatsDownCal;
    private float fatsDownGram;
    private float carbsDownCal;
    private float carbsDownGram;
    //add new data 19.03.2016
    private int somatoType;
    private int clasIMT;
    private int optimalWaist;

    public ClientCard(Context context) {
    }

    public ClientCard(
            Context context,
            String firstName,
            String lastName,
            int gender,
            String eMail,
            String telNum,
            int indexSolov,
            int height,
            int weight,
            int age,
            int waistSize) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.eMail = eMail;
        this.telNum = telNum;
        this.indexSolov = indexSolov;
        this.height = height;
        this.weight = weight;
        this.age = age;
        this.waistSize = waistSize;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getEMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public int getIndexSolov() {
        return indexSolov;
    }

    public void setIndexSolov(int indexSolov) {
        this.indexSolov = indexSolov;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWaistSize() {
        return waistSize;
    }

    public void setWaistSize(int waistSize) {
        this.waistSize = waistSize;
    }

    public float getActivityСoeff() {
        return activityСoeff;
    }

    public void setActivityСoeff(float activityСoeff) {
        this.activityСoeff = activityСoeff;
    }

    public float getDataIMT() {
        return dataIMT;
    }

    public void setDataIMT(float dataIMT) {
        this.dataIMT = dataIMT;
    }

    public float getWeightDown() {
        return weightDown;
    }

    public void setWeightDown(float weightDown) {
        this.weightDown = weightDown;
    }

    public float getWeightUp() {
        return weightUp;
    }

    public void setWeightUp(float weightUp) {
        this.weightUp = weightUp;
    }

    public float getDataBM() {
        return dataBM;
    }

    public void setDataBM(float dataBM) {
        this.dataBM = dataBM;
    }

    public float getDataOO() {
        return dataOO;
    }

    public void setDataOO(float dataOO) {
        this.dataOO = dataOO;
    }

    public float getDataMSGdown() {
        return dataMSGdown;
    }

    public void setDataMSGdown(float dataMSGdown) {
        this.dataMSGdown = dataMSGdown;
    }

    public float getDataMSGup() {
        return dataMSGup;
    }

    public void setDataMSGup(float dataMSGup) {
        this.dataMSGup = dataMSGup;
    }

    public float getProteinsNormCal() {
        return proteinsNormCal;
    }

    public void setProteinsNormCal(float proteinsNormCal) {
        this.proteinsNormCal = proteinsNormCal;
    }

    public float getProteinsNormGram() {
        return proteinsNormGram;
    }

    public void setProteinsNormGram(float proteinsNormGram) {
        this.proteinsNormGram = proteinsNormGram;
    }

    public float getFatsNormCal() {
        return fatsNormCal;
    }

    public void setFatsNormCal(float fatsNormCal) {
        this.fatsNormCal = fatsNormCal;
    }

    public float getFatsNormGram() {
        return fatsNormGram;
    }

    public void setFatsNormGram(float fatsNormGram) {
        this.fatsNormGram = fatsNormGram;
    }

    public float getCarbsNormCal() {
        return carbsNormCal;
    }

    public void setCarbsNormCal(float carbsNormCal) {
        this.carbsNormCal = carbsNormCal;
    }

    public float getCarbsNormGram() {
        return carbsNormGram;
    }

    public void setCarbsNormGram(float carbsNormGram) {
        this.carbsNormGram = carbsNormGram;
    }

    public float getProteinsDownCal() {
        return proteinsDownCal;
    }

    public void setProteinsDownCal(float proteinsDownCal) {
        this.proteinsDownCal = proteinsDownCal;
    }

    public float getProteinsDownGram() {
        return proteinsDownGram;
    }

    public void setProteinsDownGram(float proteinsDownGram) {
        this.proteinsDownGram = proteinsDownGram;
    }

    public float getFatsDownCal() {
        return fatsDownCal;
    }

    public void setFatsDownCal(float fatsDownCal) {
        this.fatsDownCal = fatsDownCal;
    }

    public float getFatsDownGram() {
        return fatsDownGram;
    }

    public void setFatsDownGram(float fatsDownGram) {
        this.fatsDownGram = fatsDownGram;
    }

    public float getCarbsDownCal() {
        return carbsDownCal;
    }

    public void setCarbsDownCal(float carbsDownCal) {
        this.carbsDownCal = carbsDownCal;
    }

    public float getCarbsDownGram() {
        return carbsDownGram;
    }

    public void setCarbsDownGram(float carbsDownGram) {
        this.carbsDownGram = carbsDownGram;
    }

    public int getSomatoType() {
        return somatoType;
    }

    public void setSomatoType(int somatoType) {
        this.somatoType = somatoType;
    }

    public int getClasIMT() {
        return clasIMT;
    }

    public void setClasIMT(int clasIMT) {
        this.clasIMT = clasIMT;
    }

    public int getOptimalWaist() {
        return optimalWaist;
    }

    public void setOptimalWaist(int optimalWaist) {
        this.optimalWaist = optimalWaist;
    }

    public void setActivityСoeffLow() {
        activityСoeff = ACTIVITY_LOW;
    }

    public void setActivityСoeffSmall() {
        activityСoeff = ACTIVITY_SMALL;
    }

    public void setActivityСoeffMiddle() {
        activityСoeff = ACTIVITY_MIDDLE;
    }

    public void setActivityСoeffHigh() {
        activityСoeff = ACTIVITY_HIGH;
    }

    public void setGenderMale() {
        gender = GENDER_MALE;
    }

    public void setGenderFemale() {
        gender = GENDER_FEMALE;
    }
}
