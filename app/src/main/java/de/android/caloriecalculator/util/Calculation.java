package de.android.caloriecalculator.util;

/*
 * Calculation.java    v.1.0 20.03.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.dto.IClientCard;

public class Calculation implements IClientCard {
    ClientCard clientCard;

    public Calculation() {
    }

    private float calculateDataIMT() {
        try {
            if (clientCard.getGender() == GENDER_MALE) {
                return 19 * clientCard.getWeight() * 10000 / (clientCard.getIndexSolov()
                        * clientCard.getHeight() * clientCard.getHeight());
            } else {
                return 16 * clientCard.getWeight() * 10000 / (clientCard.getIndexSolov()
                        * clientCard.getHeight() * clientCard.getHeight());
            }
        } catch (ArithmeticException e) {
            e.getStackTrace();
            return 0;
        }
    }

    private float calculateWeightDown() {
        try {
        if(clientCard.getGender() == GENDER_MALE) {
            return 20 * clientCard.getIndexSolov() *
                    (clientCard.getHeight() * clientCard.getHeight() / 10000) / 19;
        } else {
            return 20 * clientCard.getIndexSolov() *
                    (clientCard.getHeight() * clientCard.getHeight() / 10000) / 16;
        }
        } catch (ArithmeticException e) {
            e.getStackTrace();
            return 0;
        }
    }

    private float calculateWeightUp() {
        try {
        if(clientCard.getGender() == GENDER_MALE) {
            return 24 * clientCard.getIndexSolov() *
                    (clientCard.getHeight() * clientCard.getHeight() / 10000) / 19;
        } else {
            return 24 * clientCard.getIndexSolov() *
                    (clientCard.getHeight() * clientCard.getHeight() / 10000) / 16;
        }
        } catch (ArithmeticException e) {
            e.getStackTrace();
            return 0;
        }
    }

    private float calculateDataBM() {
        try {
        if(clientCard.getGender() == GENDER_MALE) {
            return (float) (66 + (13.7 * clientCard.getWeight()) + (5 * clientCard.getHeight())
                    - (6.8 * clientCard.getAge()));
        } else {
            return (float) (655 + (9.6 * clientCard.getWeight()) + (1.8 * clientCard.getHeight())
                    - (4.7 * clientCard.getAge()));
        }
        } catch (ArithmeticException e) {
            e.getStackTrace();
            return 0;
        }
    }

    private float calculateDataOO() {
        return clientCard.getDataBM() * clientCard.getActivityСoeff();
    }

    private float calculateDataMSGdown() {
        return clientCard.getDataOO() - 750;
    }

    private float calculateDataMSGup() {
        return clientCard.getDataOO() - 400;
    }

    private float calculateProteinsNormCal() {
        return (float) (clientCard.getDataOO() * 0.17);
    }

    private float calculateProteinsNormGram() {
        return clientCard.getProteinsNormCal() / 4;
    }

    private float calculateFatsNormCal() {
        return (float) (clientCard.getDataOO() * 0.17);
    }

    private float calculateFatsNormGram() {
        return clientCard.getFatsNormCal() / 9;
    }

    private float calculateCarbsNormCal() {
        return (float) (clientCard.getDataOO() * 0.67);
    }

    private float calculateCarbsNormGram() {
        return clientCard.getCarbsNormCal() / 4;
    }

    private float calculateProteinsDownCal() {
        return (float) (clientCard.getDataOO() * 0.25);
    }

    private float calculateProteinsDownGram() {
        return clientCard.getProteinsDownCal() / 4;
    }

    private float calculateFatsDownCal() {
        return (float) (clientCard.getDataOO() * 0.25);
    }

    private float calculateFatsDownGram() {
        return clientCard.getFatsDownCal() / 9;
    }

    private float calculateCarbsDownCal() {
        return (float) (clientCard.getDataOO() * 0.5);
    }

    private float calculateCarbsDownGram() {
        return clientCard.getCarbsDownCal() / 4;
    }

    private int calculateSomatoType() {
        if(clientCard.getGender() == GENDER_MALE) {
            if(clientCard.getIndexSolov() < 18) {
                return SOMATO_TYPE_FRAIL;
            } else if(clientCard.getIndexSolov() > 20) {
                return SOMATO_TYPE_LARGE;
            } else {
                return SOMATO_TYPE_MEDIUM;
            }
        } else {
            if(clientCard.getIndexSolov() < 15) {
                return SOMATO_TYPE_FRAIL;
            } else if(clientCard.getIndexSolov() > 17) {
                return SOMATO_TYPE_LARGE;
            } else {
                return SOMATO_TYPE_MEDIUM;
            }
        }
    }

    private int calculateClasIMT() {
        float dataIMT = clientCard.getDataIMT();

        if(dataIMT < 16) {
            return LARGE_UNDER_WEIGHT;
        } else if( (dataIMT >= 16) & (dataIMT < 17) ) {
            return MILD_UNDER_WEIGHT;
        } else if( (dataIMT >= 17) & (dataIMT < 18) ) {
            return SMALL_UNDER_WEIGHT;
        } else if( (dataIMT >= 18) & (dataIMT < 18.5) ) {
            return UNDER_WEIGHT;
        } else if( (dataIMT >= 18.5) & (dataIMT < 25) ) {
            return NORMAL_WEIGHT;
        } else if( (dataIMT >= 25) & (dataIMT < 26) ) {
            return OVER_WEIGHT;
        } else if( (dataIMT >= 26) & (dataIMT < 30) ) {
            return MILD_OVER_WEIGHT;
        } else {
            return LARGE_OVER_WEIGHT;
        }
    }

    private int calculateOptimalWaist() {
        try {
        if(clientCard.getGender() == GENDER_MALE) {
            return clientCard.getHeight() / 2 - 5;
        } else {
            return clientCard.getHeight() / 2 - 15;
        }
        } catch (ArithmeticException e) {
            e.getStackTrace();
            return 0;
        }
    }

    public void calculate(ClientCard clientCard) {
        this.clientCard = clientCard;

        clientCard.setDataIMT(calculateDataIMT());
        clientCard.setWeightDown(calculateWeightDown());
        clientCard.setWeightUp(calculateWeightUp());
        clientCard.setDataBM(calculateDataBM());
        clientCard.setDataOO(calculateDataOO());
        clientCard.setDataMSGdown(calculateDataMSGdown());
        clientCard.setDataMSGup(calculateDataMSGup());
        clientCard.setProteinsNormCal(calculateProteinsNormCal());
        clientCard.setProteinsNormGram(calculateProteinsNormGram());
        clientCard.setFatsNormCal(calculateFatsNormCal());
        clientCard.setFatsNormGram(calculateFatsNormGram());
        clientCard.setCarbsNormCal(calculateCarbsNormCal());
        clientCard.setCarbsNormGram(calculateCarbsNormGram());
        clientCard.setProteinsDownCal(calculateProteinsDownCal());
        clientCard.setProteinsDownGram(calculateProteinsDownGram());
        clientCard.setFatsDownCal(calculateFatsDownCal());
        clientCard.setFatsDownGram(calculateFatsDownGram());
        clientCard.setCarbsDownCal(calculateCarbsDownCal());
        clientCard.setCarbsDownGram(calculateCarbsDownGram());
        clientCard.setSomatoType(calculateSomatoType());
        clientCard.setClasIMT(calculateClasIMT());
        clientCard.setOptimalWaist(calculateOptimalWaist());
    }
}
