package de.android.caloriecalculator.util;

/*
 * Result.java    v.1.0 23.03.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

import android.content.Context;

import de.android.caloriecalculator.R;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.dto.IClientCard;

public class Result implements IClientCard{
    ClientCard clientCard;
    Context context;

    public Result(ClientCard clientCard, Context context) {
        this.clientCard = clientCard;
        this.context = context;
    }

    public Result(Context context) {
        this.context = context;
    }

    public String fetchActivity(float activityСoeff) {
        if(activityСoeff == ACTIVITY_LOW) {
            return context.getResources().getString(R.string.activity_low);
        } else if(activityСoeff == ACTIVITY_SMALL) {
            return context.getResources().getString(R.string.activity_small);
        } else if(activityСoeff == ACTIVITY_MIDDLE) {
            return context.getResources().getString(R.string.activity_middle);
        } else if(activityСoeff == ACTIVITY_HIGH) {
            return context.getResources().getString(R.string.activity_high);
        }
        return "";
    }

    public String fetchActivity() {
        float activityСoeff = clientCard.getActivityСoeff();

        if(activityСoeff == ACTIVITY_LOW) {
            return context.getResources().getString(R.string.activity_low);
        } else if(activityСoeff == ACTIVITY_SMALL) {
            return context.getResources().getString(R.string.activity_small);
        } else if(activityСoeff == ACTIVITY_MIDDLE) {
            return context.getResources().getString(R.string.activity_middle);
        } else if(activityСoeff == ACTIVITY_HIGH) {
            return context.getResources().getString(R.string.activity_high);
        }
        return "";
    }

    public String fetchGenderType() {
        int genderType = clientCard.getGender();

        switch (genderType) {
            case GENDER_MALE:
                return context.getResources().getString(R.string.male);
            case GENDER_FEMALE:
                return context.getResources().getString(R.string.female);
        }
        return "";
    }

    public String fetchSomatoType() {
        int somatoType = clientCard.getSomatoType();

        switch (somatoType) {
            case SOMATO_TYPE_FRAIL:
                return context.getResources().getString(R.string.SOMATO_TYPE_FRAIL);
            case SOMATO_TYPE_MEDIUM:
                return context.getResources().getString(R.string.SOMATO_TYPE_MEDIUM);
            case SOMATO_TYPE_LARGE:
                return context.getResources().getString(R.string.SOMATO_TYPE_LARGE);
        }
        return "";
    }

    public String fetchIMTclassification() {
        int clasIMT = clientCard.getClasIMT();

        switch (clasIMT) {
            case LARGE_UNDER_WEIGHT:
                return context.getResources().getString(R.string.LARGE_UNDER_WEIGHT);
            case MILD_UNDER_WEIGHT:
                return context.getResources().getString(R.string.MILD_UNDER_WEIGHT);
            case SMALL_UNDER_WEIGHT:
                return context.getResources().getString(R.string.SMALL_UNDER_WEIGHT);
            case UNDER_WEIGHT:
                return context.getResources().getString(R.string.UNDER_WEIGHT);
            case NORMAL_WEIGHT:
                return context.getResources().getString(R.string.NORMAL_WEIGHT);
            case OVER_WEIGHT:
                return context.getResources().getString(R.string.OVER_WEIGHT);
            case MILD_OVER_WEIGHT:
                return context.getResources().getString(R.string.MILD_OVER_WEIGHT);
            case LARGE_OVER_WEIGHT:
                return context.getResources().getString(R.string.LARGE_OVER_WEIGHT);
        }
        return "";
    }

    public String fetchIMTrisk() {
        int clasIMT = clientCard.getClasIMT();

        switch (clasIMT) {
            case LARGE_UNDER_WEIGHT:
                return context.getResources().getString(R.string.LARGE_UNDER_WEIGHT_RISK);
            case MILD_UNDER_WEIGHT:
                return context.getResources().getString(R.string.MILD_UNDER_WEIGHT_RISK);
            case SMALL_UNDER_WEIGHT:
                return context.getResources().getString(R.string.SMALL_UNDER_WEIGHT_RISK);
            case UNDER_WEIGHT:
                return context.getResources().getString(R.string.UNDER_WEIGHT_RISK);
            case NORMAL_WEIGHT:
                return context.getResources().getString(R.string.NORMAL_WEIGHT_RISK);
            case OVER_WEIGHT:
                return context.getResources().getString(R.string.OVER_WEIGHT_RISK);
            case MILD_OVER_WEIGHT:
                return context.getResources().getString(R.string.MILD_OVER_WEIGHT_RISK);
            case LARGE_OVER_WEIGHT:
                return context.getResources().getString(R.string.LARGE_OVER_WEIGHT_RISK);
        }
        return "";
    }

    public String fetchRecommendationMeal() {
        int somatoType = clientCard.getSomatoType();
        String[] stringsRecommendation = new String[0];
        StringBuilder stringBuilder = new StringBuilder();

         switch (somatoType) {
             case SOMATO_TYPE_FRAIL:
                 stringsRecommendation = context.getResources().
                         getStringArray(R.array.frailSomatoType_RecommendationMeal);
                 break;
             case SOMATO_TYPE_MEDIUM:
                 stringsRecommendation = context.getResources().
                         getStringArray(R.array.mediumSomatoType_RecommendationMeal);
                 break;
             case SOMATO_TYPE_LARGE:
                 stringsRecommendation = context.getResources().
                         getStringArray(R.array.largeSomatoType_RecommendationMeal);
                 break;
         }

        for (String aStringsRecommendation : stringsRecommendation) {
            stringBuilder.append("- ").append(aStringsRecommendation).append("\n");
        }

        return stringBuilder.toString();
    }

    public String fetchRecommendationNotMeal() {
        int somatoType = clientCard.getSomatoType();
        String[] stringsRecommendation = new String[0];
        StringBuilder stringBuilder = new StringBuilder();

        switch (somatoType) {
            case SOMATO_TYPE_FRAIL:
                stringsRecommendation = context.getResources().
                        getStringArray(R.array.frailSomatoType_RecommendationNotMeal);
                break;
            case SOMATO_TYPE_MEDIUM:
                stringsRecommendation = context.getResources().
                        getStringArray(R.array.mediumSomatoType_RecommendationNotMeal);
                break;
            case SOMATO_TYPE_LARGE:
                stringsRecommendation = context.getResources().
                        getStringArray(R.array.largeSomatoType_RecommendationNotMeal);
                break;
        }

        for (String aStringsRecommendation : stringsRecommendation) {
            stringBuilder.append("- ").append(aStringsRecommendation).append("\n");
        }

        return stringBuilder.toString();
    }
}
