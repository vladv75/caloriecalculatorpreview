package de.android.caloriecalculator.util;

import android.content.Context;
import android.content.SharedPreferences;

import de.android.caloriecalculator.dto.ClientCard;

/*
 * Utils.java    v.1.1 28.06.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class Utils {
    public static String CURRENT_MANAGER_PREF = "manager_pref";
    public static String INFO_PREF = "info_pref";

    public static final String COLUMN_MANAGER_FIRST_NAME = "first_name";
    public static final String COLUMN_MANAGER_LAST_NAME = "last_name";
    public static final String COLUMN_MANAGER_EMAIL = "email";
    public static final String COLUMN_MANAGER_TELEPHONE_NUMBER = "telephone_number";
    public static final String COLUMN_MANAGER_INDEX = "index";
    public static final String COLUMN_MANAGER_HEIGHT = "height";
    public static final String COLUMN_MANAGER_WEIGHT = "weight";
    public static final String COLUMN_MANAGER_AGE = "age";
    public static final String COLUMN_MANAGER_WAIST_SIZE = "waist_size";
    public static final String COLUMN_MANAGER_ACTIVITY = "activity";
    public static final String COLUMN_MANAGER_GENDER = "gender";
    public static final String COLUMN_MANAGER_SOMATOTYPE = "somatoType";

    public static final String  INFO_TOUR_SHOWN = "tour_shown";
    public static final String  INFO_REGISTRATION_PASSED = "registration_passed";

    private static ClientCard currentManagerCard;
    private static Boolean infoTourShown;
    private static Boolean infoRegistrationPassed;

    public static ClientCard getCurrentManagerCard(Context context) {
        if (currentManagerCard == null) {
            SharedPreferences preferences = context.getSharedPreferences(CURRENT_MANAGER_PREF, 0);
            currentManagerCard = new ClientCard(context);
            currentManagerCard.setFirstName(preferences.getString(COLUMN_MANAGER_FIRST_NAME, ""));
            currentManagerCard.setLastName(preferences.getString(COLUMN_MANAGER_LAST_NAME, ""));
            currentManagerCard.seteMail(preferences.getString(COLUMN_MANAGER_EMAIL, ""));
            currentManagerCard.setTelNum(preferences.getString(COLUMN_MANAGER_TELEPHONE_NUMBER, ""));
            currentManagerCard.setGender(preferences.getInt(COLUMN_MANAGER_GENDER, 0));
            currentManagerCard.setIndexSolov(preferences.getInt(COLUMN_MANAGER_INDEX, 0));
            currentManagerCard.setHeight(preferences.getInt(COLUMN_MANAGER_HEIGHT, 0));
            currentManagerCard.setWeight(preferences.getInt(COLUMN_MANAGER_WEIGHT, 0));
            currentManagerCard.setAge(preferences.getInt(COLUMN_MANAGER_AGE, 0));
            currentManagerCard.setWaistSize(preferences.getInt(COLUMN_MANAGER_WAIST_SIZE, 0));
            currentManagerCard.setActivityСoeff(preferences.getFloat(COLUMN_MANAGER_ACTIVITY, 0));
            currentManagerCard.setSomatoType(preferences.getInt(COLUMN_MANAGER_SOMATOTYPE, 0));
        }
        return currentManagerCard;
    }

    public static void setCurrentManagerCard(Context context, ClientCard managerCard) {
        currentManagerCard = managerCard;
        SharedPreferences preferences = context.getSharedPreferences(CURRENT_MANAGER_PREF, 0);
        SharedPreferences.Editor editor = preferences.edit();
        if (currentManagerCard != null) {
            editor.putString(COLUMN_MANAGER_FIRST_NAME, managerCard.getFirstName());
            editor.putString(COLUMN_MANAGER_LAST_NAME, managerCard.getLastName());
            editor.putString(COLUMN_MANAGER_EMAIL, managerCard.getEMail());
            editor.putString(COLUMN_MANAGER_TELEPHONE_NUMBER, managerCard.getTelNum());
            editor.putInt(COLUMN_MANAGER_GENDER, managerCard.getGender());
            editor.putInt(COLUMN_MANAGER_INDEX, managerCard.getIndexSolov());
            editor.putInt(COLUMN_MANAGER_HEIGHT, managerCard.getHeight());
            editor.putInt(COLUMN_MANAGER_WEIGHT, managerCard.getWeight());
            editor.putInt(COLUMN_MANAGER_AGE, managerCard.getAge());
            editor.putInt(COLUMN_MANAGER_WAIST_SIZE, managerCard.getWaistSize());
            editor.putFloat(COLUMN_MANAGER_ACTIVITY, managerCard.getActivityСoeff());
            editor.putInt(COLUMN_MANAGER_SOMATOTYPE, managerCard.getSomatoType());
        } else {
            editor.putInt("user_id", 0);
        }
        editor.apply();
    }

    public static Boolean getInfoTourShown(Context context) {
        if (infoTourShown == null) {
            SharedPreferences preferences = context.getSharedPreferences(INFO_PREF, 0);
            infoTourShown = preferences.getBoolean(INFO_TOUR_SHOWN, false);
        }
        return infoTourShown;
    }

    public static void setInfoTourShown(Context context,Boolean infoTourShownValue) {
        infoTourShown = infoTourShownValue;
        SharedPreferences preferences = context.getSharedPreferences(INFO_PREF, 0);
        SharedPreferences.Editor editor = preferences.edit();
        if (infoTourShown != null) {
            editor.putBoolean(INFO_TOUR_SHOWN, infoTourShownValue);
        }
        editor.apply();
    }

    public static Boolean getInfoRegistrationPassed(Context context) {
        if (infoRegistrationPassed == null) {
            SharedPreferences preferences = context.getSharedPreferences(INFO_PREF, 0);
            infoRegistrationPassed = preferences.getBoolean(INFO_REGISTRATION_PASSED, false);
        }
        return infoRegistrationPassed;
    }

    public static void setInfoRegistrationPassed(Context context, Boolean infoRegistrationPassedValue) {
        infoRegistrationPassed = infoRegistrationPassedValue;
        SharedPreferences preferences = context.getSharedPreferences(INFO_PREF, 0);
        SharedPreferences.Editor editor = preferences.edit();
        if (infoRegistrationPassed != null) {
            editor.putBoolean(INFO_REGISTRATION_PASSED, infoRegistrationPassedValue);
        }
        editor.apply();
    }
}
