package de.android.caloriecalculator.util;

import android.content.Context;
import android.os.Environment;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

import de.android.caloriecalculator.R;
import de.android.caloriecalculator.dto.ClientCard;

/*
 * PdfUtil.java    v.1.1 01.07.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class PdfUtil {
    private static String FILE = Environment.getExternalStorageDirectory().getAbsolutePath()
            + "/CalorieCalculatorPdfReport.pdf";
    public static Context context;
    public static String FIRST_NAME_FIELD;
    public static String LAST_NAME_FIELD;
    public static String AGE_FIELD;
    public static String HEIGHT_FIELD;
    public static String WEIGHT_FIELD;
    public static String WAIST_SIZE_FIELD;
    public static String ACTIVITY_COEFF;

    private static ClientCard clientCard;
    private static Result result;

    private static Font cFont;
    private static Font sFont;
    private static Font sbFont;
    private static Font ssbFont;
    private static Font cbFont;
    private static Document document;

    public static void init() {
        try {
            BaseFont bf = BaseFont.createFont("assets/arial.ttf",
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            cFont = new Font(bf, 22, Font.BOLD);
            cbFont = new Font(bf, 18, Font.BOLD);
            sFont = new Font(bf, 14, Font.NORMAL);
            sbFont = new Font(bf, 14, Font.BOLD);
            ssbFont = new Font(bf, 12, Font.BOLD);
        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        }
        FIRST_NAME_FIELD = context.getString(R.string.name) + ":";
        LAST_NAME_FIELD = context.getString(R.string.last_name) + ":";
        AGE_FIELD = context.getString(R.string.age) + ":";
        HEIGHT_FIELD = context.getString(R.string.height) + ":";
        WEIGHT_FIELD = context.getString(R.string.weight) + ":";
        WAIST_SIZE_FIELD = context.getString(R.string.waist_size) + ":";
        ACTIVITY_COEFF = context.getString(R.string.activity_coefficient) + ":";
    }

    public static String createPdfFromClient(Context contextValue, ClientCard clientCardValue) {
        context = contextValue;
        clientCard = clientCardValue;
        result = new Result(clientCard, context);
        init();
        createDocumentFile();
        try {
            addMetaData(document);
            addContent(document);
            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return FILE;
    }

    public static void addContent(Document document) throws DocumentException {
        addString(document, context.getString(R.string.app_report), cFont, Element.ALIGN_CENTER);
        addString(document, context.getString(R.string.health_calculator), cFont, Element.ALIGN_CENTER);
        addString(document, context.getString(R.string.page_1), sFont, Element.ALIGN_CENTER);
        addEmptyLine(document, 1);

        addString(document, context.getString(R.string.questionairre), cbFont, Element.ALIGN_CENTER);
        addEmptyLine(document, 1);

        createTableProfile(document);
        addEmptyLine(document, 1);

        addString(document, context.getString(R.string.result_calc), cbFont, Element.ALIGN_CENTER);
        List list = new List(false, false, 6);
        list.add(new ListItem(context.getString(R.string.physique) + " " + result.fetchSomatoType(), sFont));
        list.add(new ListItem(context.getString(R.string.imt_class) + " " + result.fetchIMTclassification(), sFont));
        list.add(new ListItem(context.getString(R.string.imt_risk) + " " + result.fetchIMTrisk(), sFont));
        document.add(list);
        addEmptyLine(document, 1);

        createTableResult(document);
        addEmptyLine(document, 1);

        addString(document, context.getString(R.string.balance_in_normal), sFont, Element.ALIGN_LEFT);
        addEmptyLine(document, 1);
        createTableBGUnorm(document);
        addEmptyLine(document, 1);

        addString(document, context.getString(R.string.balance_by_reducing_the_weight), sFont, Element.ALIGN_LEFT);
        addEmptyLine(document, 1);
        createTableBGUdown(document);
        addEmptyLine(document, 1);

        // Start a new page
        document.newPage();

        addString(document, context.getString(R.string.app_report), cFont, Element.ALIGN_CENTER);
        addString(document, context.getString(R.string.health_calculator), cFont, Element.ALIGN_CENTER);
        addString(document, context.getString(R.string.page_2), sFont, Element.ALIGN_CENTER);
        addEmptyLine(document, 1);

        addString(document, context.getString(R.string.recommendations), cbFont, Element.ALIGN_CENTER);
        addEmptyLine(document, 1);

        addString(document, context.getString(R.string.preferred_foods_and_diet), sbFont, Element.ALIGN_LEFT);
        addString(document, result.fetchRecommendationMeal(), sFont, Element.ALIGN_LEFT);
        addEmptyLine(document, 1);

        addString(document, context.getString(R.string.it_is_desirable_to_limit_exclude), sbFont, Element.ALIGN_LEFT);
        addString(document, result.fetchRecommendationNotMeal(), sFont, Element.ALIGN_LEFT);
        addEmptyLine(document, 1);

        ClientCard userCard = Utils.getCurrentManagerCard(context);
        addString(document, context.getString(R.string.consultant), sbFont, Element.ALIGN_RIGHT);
        addString(document, userCard.getFirstName() + " " + userCard.getLastName() + "\n"
                        + context.getString(R.string.telephone) + ": " + userCard.getTelNum() + "\n"
                        + context.getString(R.string.email) + ": "  + userCard.getEMail(),
                sFont, Element.ALIGN_RIGHT);
        addEmptyLine(document, 1);

        addString(document, context.getString(R.string.footer), ssbFont, Element.ALIGN_LEFT);
    }

    private static void createDocumentFile() {
        document = new Document();
        document.setPageSize(PageSize.A4);
        document.setMargins(40, 40, 10, 10);
        try {
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void addMetaData(Document document) {
        document.addTitle(context.getString(R.string.title_pdf));
        document.addSubject(context.getString(R.string.subject_pdf));
        document.addKeywords(context.getString(R.string.keywords_pdf));
        document.addAuthor(context.getString(R.string.author_pdf));
        document.addCreator(context.getString(R.string.author_pdf));
    }

    private static void addString(Document document, String string, Font cellFont, int alignment)
            throws DocumentException {
        Paragraph paragraph;
        paragraph = new Paragraph(string, cellFont);
        paragraph.setAlignment(alignment);
        document.add(paragraph);
    }

    public static void createTableProfile(Document document) throws DocumentException {
        PdfPTable table = new PdfPTable(2);

        table.addCell(new PdfPCell(new Phrase(FIRST_NAME_FIELD, sFont)));
        table.addCell((new PdfPCell(new Phrase(clientCard.getFirstName(), sbFont))));

        table.addCell((new PdfPCell(new Phrase(LAST_NAME_FIELD, sFont))));
        table.addCell((new PdfPCell(new Phrase(clientCard.getLastName(), sbFont))));

        table.addCell((new PdfPCell(new Phrase(AGE_FIELD, sFont))));
        table.addCell((new PdfPCell(new Phrase(String.valueOf(clientCard.getAge()), sbFont))));

        table.addCell((new PdfPCell(new Phrase(HEIGHT_FIELD, sFont))));
        table.addCell((new PdfPCell(new Phrase(String.valueOf(clientCard.getHeight()), sbFont))));

        table.addCell((new PdfPCell(new Phrase(WEIGHT_FIELD, sFont))));
        table.addCell((new PdfPCell(new Phrase(String.valueOf(clientCard.getWeight()), sbFont))));

        table.addCell((new PdfPCell(new Phrase(WAIST_SIZE_FIELD, sFont))));
        table.addCell((new PdfPCell(new Phrase(String.valueOf(clientCard.getWaistSize()), sbFont))));

        table.addCell((new PdfPCell(new Phrase(ACTIVITY_COEFF, sFont))));
        table.addCell((new PdfPCell(new Phrase(String.valueOf(clientCard.getActivityСoeff()
                + " (" + result.fetchActivity() + ")"), sbFont))));

        document.add(table);
    }

    public static void createTableResult(Document document) throws DocumentException {
        PdfPTable table = new PdfPTable(4);

        table.addCell(new PdfPCell(new Phrase(context.getString(R.string.imt_iss), sFont)));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getDataIMT()), sbFont))));
        table.addCell((new PdfPCell(new Phrase(context.getString(R.string.up_down_limits), sFont))));
        table.addCell((new PdfPCell(new Phrase(
                String.format(Locale.GERMANY, "%.2f", clientCard.getWeightDown()) + " " +
                        context.getString(R.string.kg) +
                        "\n" +
                        String.format(Locale.GERMANY, "%.2f", clientCard.getWeightUp()) + " " +
                        context.getString(R.string.kg), sbFont
        ))));

        table.addCell(new PdfPCell(new Phrase(context.getString(R.string.basic_metabolism), sFont)));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getDataBM()), sbFont))));
        table.addCell((new PdfPCell(new Phrase(context.getString(R.string.MSG_lower_bound), sFont))));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getDataMSGdown()), sbFont))));

        table.addCell(new PdfPCell(new Phrase(context.getString(R.string.main_volume), sFont)));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getDataOO()), sbFont))));
        table.addCell((new PdfPCell(new Phrase(context.getString(R.string.MSG_upper_bound), sFont))));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getDataMSGup()), sbFont))));

        document.add(table);
    }

    public static void createTableBGUnorm(Document document) throws DocumentException {
        PdfPTable table = new PdfPTable(3);

        table.addCell(new PdfPCell(new Phrase("", sFont)));
        table.addCell((new PdfPCell(new Phrase(context.getString(R.string.call), sFont))));
        table.addCell((new PdfPCell(new Phrase(context.getString(R.string.gram), sFont))));

        table.addCell(new PdfPCell(new Phrase(context.getString(R.string.proteins), sFont)));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getProteinsNormCal()), sbFont))));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getProteinsNormGram()), sbFont))));

        table.addCell(new PdfPCell(new Phrase(context.getString(R.string.fat), sFont)));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getFatsNormCal()), sbFont))));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getFatsNormGram()), sbFont))));

        table.addCell(new PdfPCell(new Phrase(context.getString(R.string.carbohydrates), sFont)));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getCarbsNormCal()), sbFont))));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getCarbsNormGram()), sbFont))));

        document.add(table);
    }

    public static void createTableBGUdown(Document document) throws DocumentException {
        PdfPTable table = new PdfPTable(3);

        table.addCell(new PdfPCell(new Phrase("", sFont)));
        table.addCell((new PdfPCell(new Phrase(context.getString(R.string.call), sFont))));
        table.addCell((new PdfPCell(new Phrase(context.getString(R.string.gram), sFont))));

        table.addCell(new PdfPCell(new Phrase(context.getString(R.string.proteins), sFont)));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getProteinsDownCal()), sbFont))));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getProteinsDownGram()), sbFont))));

        table.addCell(new PdfPCell(new Phrase(context.getString(R.string.fat), sFont)));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getFatsDownCal()), sbFont))));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getFatsDownGram()), sbFont))));

        table.addCell(new PdfPCell(new Phrase(context.getString(R.string.carbohydrates), sFont)));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getCarbsDownCal()), sbFont))));
        table.addCell((new PdfPCell(new Phrase(String.format(Locale.GERMANY, "%.2f", clientCard.getCarbsDownGram()), sbFont))));

        document.add(table);
    }

    public static void addEmptyLine(Document document, int number) throws DocumentException {
        for (int i = 0; i < number; i++) {
            document.add(new Paragraph(" "));
        }
    }
}
