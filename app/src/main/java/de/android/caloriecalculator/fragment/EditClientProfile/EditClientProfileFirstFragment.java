package de.android.caloriecalculator.fragment.EditClientProfile;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import de.android.caloriecalculator.R;
import de.android.caloriecalculator.activity.EditClientProfileActivity;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.dto.IClientCard;

import static de.android.caloriecalculator.activity.EditClientProfileActivity.FRAGMENT_SECOND;

/*
 * EditClientProfileFirstFragment.java    v.1.0 03.04.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class EditClientProfileFirstFragment extends Fragment
        implements IClientCard, Validator.ValidationListener {
    @NotEmpty @Length(min = 1, max = 30)
    EditText etName;
    @NotEmpty @Length(min = 1, max = 30)
    EditText etLastName;
    @NotEmpty @Email EditText etEmail;
    @NotEmpty
    EditText etTelephoneNumber;
    RadioButton rbMale;
    RadioButton rbFemale;
    RadioGroup rgGender;

    private Validator validator;
    private ClientCard clientCard;
    private EditClientProfileActivity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_editclientprofile_first, container, false);

        validator = new Validator(this);
        validator.setValidationListener(this);

        FloatingActionButton floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

        setupData(view);
        return view;
    }

    private void setupData(View view) {
        activity = (EditClientProfileActivity) getActivity();
        clientCard = activity.takeClientCard();

        etName = (EditText) view.findViewById(R.id.etName);
        etLastName = (EditText) view.findViewById(R.id.etLastName);
        etEmail = (EditText) view.findViewById(R.id.etEmail);
        etTelephoneNumber = (EditText) view.findViewById(R.id.etTelephoneNumber);
        rbMale = (RadioButton) view.findViewById(R.id.rbMale);
        rbFemale = (RadioButton) view.findViewById(R.id.rbFemale);
        rgGender = (RadioGroup) view.findViewById(R.id.rgGender);

        etName.setText(clientCard.getFirstName());
        etLastName.setText(clientCard.getLastName());
        etEmail.setText(clientCard.getEMail());
        etTelephoneNumber.setText(clientCard.getTelNum());

        int gender = clientCard.getGender();
        switch (gender) {
            case GENDER_MALE:
                rbMale.setChecked(true);
                break;
            case GENDER_FEMALE:
                rbFemale.setChecked(true);
                break;
        }
    }

    private void saveData() {
        clientCard.setFirstName(etName.getText().toString());
        clientCard.setLastName(etLastName.getText().toString());
        clientCard.seteMail(etEmail.getText().toString());
        clientCard.setTelNum(etTelephoneNumber.getText().toString());

        switch (rgGender.getCheckedRadioButtonId()) {
            case R.id.rbMale:
                clientCard.setGenderMale();
                break;
            case R.id.rbFemale:
                clientCard.setGenderFemale();
                break;
        }

        activity.putClientCard(clientCard);
        activity.tvFIO.setText(String.format("%s %s", clientCard.getFirstName(), clientCard.getLastName()));
        activity.tvEmail.setText(clientCard.getEMail());
    }

    @Override
    public void onValidationSucceeded() {
        saveData();
        EditClientProfileSecondFragment fragmentEditclientprofile = new EditClientProfileSecondFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragmentEditclientprofile);
        fragmentTransaction.addToBackStack("EditClientProfileSecondFragment");
        activity.fragmentType = FRAGMENT_SECOND;
        fragmentTransaction.commit();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
