package de.android.caloriecalculator.fragment.ResultCalculation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import de.android.caloriecalculator.R;
import de.android.caloriecalculator.activity.ResultCalculationActivity;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.util.Result;

/*
 * ProfileFragment.java    v.1.0 01.04.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class ProfileFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        setupResultData(view);
        return view;
    }

    private void setupResultData(View view) {
        ResultCalculationActivity activity = (ResultCalculationActivity) getActivity();
        ClientCard clientCard = activity.takeClientCard();
        Result result = new Result(clientCard, activity);

        EditText et_name = (EditText) view.findViewById(R.id.et_name);
        et_name.setText(clientCard.getFirstName());

        EditText et_last_name = (EditText) view.findViewById(R.id.et_last_name);
        et_last_name.setText(clientCard.getLastName());

        EditText et_email = (EditText) view.findViewById(R.id.et_email);
        et_email.setText(clientCard.getEMail());

        EditText et_telephone_number = (EditText) view.findViewById(R.id.et_telephone_number);
        et_telephone_number.setText(clientCard.getTelNum());

        EditText et_index = (EditText) view.findViewById(R.id.et_index);
        et_index.setText(Integer.toString(clientCard.getIndexSolov()));

        EditText et_height = (EditText) view.findViewById(R.id.et_height);
        et_height.setText(Integer.toString(clientCard.getHeight()));

        EditText et_weight = (EditText) view.findViewById(R.id.et_weight);
        et_weight.setText(Integer.toString(clientCard.getWeight()));

        EditText et_age = (EditText) view.findViewById(R.id.et_age);
        et_age.setText(Integer.toString(clientCard.getAge()));

        EditText et_waist_size = (EditText) view.findViewById(R.id.et_waist_size);
        et_waist_size.setText(Integer.toString(clientCard.getWaistSize()));

        EditText et_activity = (EditText) view.findViewById(R.id.et_activity);
        et_activity.setText(result.fetchActivity());

        EditText et_gender = (EditText) view.findViewById(R.id.et_gender);
        et_gender.setText(result.fetchGenderType());
    }

    public static Fragment newInstance() {
        return new ProfileFragment();
    }
}