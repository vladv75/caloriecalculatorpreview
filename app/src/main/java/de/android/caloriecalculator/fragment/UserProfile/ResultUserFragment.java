package de.android.caloriecalculator.fragment.UserProfile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.android.caloriecalculator.R;
import de.android.caloriecalculator.activity.UserProfileActivity;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.util.Calculation;
import de.android.caloriecalculator.util.Result;

/*
 * ResultUserFragment.java    v.1.1 28.06.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class ResultUserFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_result, container, false);
        setupResultData(view);
        return view;
    }

    private void setupResultData(View view) {
        UserProfileActivity activity = (UserProfileActivity) getActivity();
        ClientCard managerCard = activity.takeManagerCard();
        Calculation calc = new Calculation();
        calc.calculate(managerCard);
        Result result = new Result(managerCard, activity);

        TextView tvSomatoType = (TextView) view.findViewById(R.id.tvSomatoType);
        tvSomatoType.setText(result.fetchSomatoType());

        TextView tvRisk = (TextView) view.findViewById(R.id.tvRisk);
        tvRisk.setText(result.fetchIMTrisk());

        TextView tvIMT = (TextView) view.findViewById(R.id.tvIMT);
        tvIMT.setText(String.format("%s - %s",
                managerCard.getDataIMT(), result.fetchIMTclassification()));

        TextView tvWeight = (TextView) view.findViewById(R.id.tvWeight);
        tvWeight.setText(String.format("%s / %s",
                managerCard.getWeightDown(), managerCard.getWeightUp()));

        TextView tvBM = (TextView) view.findViewById(R.id.tvBM);
        tvBM.setText(String.format("%s", managerCard.getDataBM()));

        TextView tvOO = (TextView) view.findViewById(R.id.tvOO);
        tvOO.setText(String.format("%.2f", managerCard.getDataOO()));

        TextView tvMSG = (TextView) view.findViewById(R.id.tvMSG);
        tvMSG.setText(String.format("%s / %s",
                String.format("%.2f", managerCard.getDataMSGdown()),
                String.format("%.2f", managerCard.getDataMSGup())));

        TextView tvBGU = (TextView) view.findViewById(R.id.tvBGU);
        tvBGU.setText(getString(R.string.balance_in_normal));

        TextView tvProteinsNorm = (TextView) view.findViewById(R.id.tvProteinsNorm);
        tvProteinsNorm.setText(String.format("%s / %s",
                String.format("%.2f", managerCard.getProteinsNormCal()),
                String.format("%.2f", managerCard.getProteinsNormGram())));

        TextView tvFatsNorm = (TextView) view.findViewById(R.id.tvFatsNorm);
        tvFatsNorm.setText(String.format("%s / %s",
                String.format("%.2f", managerCard.getFatsNormCal()),
                String.format("%.2f", managerCard.getFatsNormGram())));

        TextView tvCarbsNorm = (TextView) view.findViewById(R.id.tvCarbsNorm);
        tvCarbsNorm.setText(String.format("%s / %s",
                String.format("%.2f", managerCard.getCarbsNormCal()),
                String.format("%.2f", managerCard.getCarbsNormGram())));

        TextView tvBGUdown = (TextView) view.findViewById(R.id.tvBGUdown);
        tvBGUdown.setText(getString(R.string.balance_by_reducing_the_weight));

        TextView tvProteinsDown = (TextView) view.findViewById(R.id.tvProteinsDown);
        tvProteinsDown.setText(String.format("%s / %s",
                String.format("%.2f", managerCard.getProteinsDownCal()),
                String.format("%.2f", managerCard.getProteinsDownGram())));

        TextView tvFatsDown = (TextView) view.findViewById(R.id.tvFatsDown);
        tvFatsDown.setText(String.format("%s / %s",
                String.format("%.2f", managerCard.getFatsDownCal()),
                String.format("%.2f", managerCard.getFatsDownGram())));

        TextView tvCarbsDown = (TextView) view.findViewById(R.id.tvCarbsDown);
        tvCarbsDown.setText(String.format("%s / %s",
                String.format("%.2f", managerCard.getCarbsDownCal()),
                String.format("%.2f", managerCard.getCarbsDownGram())));
    }

    public static Fragment newInstance() {
        return new ResultUserFragment();
    }
}
