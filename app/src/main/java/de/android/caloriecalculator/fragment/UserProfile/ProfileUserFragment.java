package de.android.caloriecalculator.fragment.UserProfile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import de.android.caloriecalculator.R;
import de.android.caloriecalculator.activity.UserProfileActivity;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.util.Result;

/*
 * ProfileFragment.java    v.1.0 01.04.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class ProfileUserFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        setupResultData(view);
        return view;
    }

    private void setupResultData(View view) {
        UserProfileActivity activity = (UserProfileActivity) getActivity();
        ClientCard managerCard = activity.takeManagerCard();
        Result result = new Result(managerCard, activity);

        EditText et_index = (EditText) view.findViewById(R.id.et_index);
        et_index.setText(Integer.toString(managerCard.getIndexSolov()));

        EditText et_height = (EditText) view.findViewById(R.id.et_height);
        et_height.setText(Integer.toString(managerCard.getHeight()));

        EditText et_weight = (EditText) view.findViewById(R.id.et_weight);
        et_weight.setText(Integer.toString(managerCard.getWeight()));

        EditText et_age = (EditText) view.findViewById(R.id.et_age);
        et_age.setText(Integer.toString(managerCard.getAge()));

        EditText et_waist_size = (EditText) view.findViewById(R.id.et_waist_size);
        et_waist_size.setText(Integer.toString(managerCard.getWaistSize()));

        EditText et_activity = (EditText) view.findViewById(R.id.et_activity);
        et_activity.setText(result.fetchActivity());

        EditText et_gender = (EditText) view.findViewById(R.id.et_gender);
        et_gender.setText(result.fetchGenderType());
    }

    public static Fragment newInstance() {
        return new ProfileUserFragment();
    }
}