package de.android.caloriecalculator.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.cengalabs.flatui.FlatUI;
import com.cengalabs.flatui.views.FlatButton;
import com.creativityapps.gmailbackgroundlibrary.BackgroundMail;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.android.caloriecalculator.R;

/*
 * FeedbackFragment.java    v.1.1 30.06.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class FeedbackFragment extends Fragment implements Validator.ValidationListener {

    @Bind(R.id.send_button) FlatButton sendButton;

    @NotEmpty
    @Bind(R.id.ETcomment) EditText commentEditText;

    private Validator validator;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);

        ButterKnife.bind(this,view);

        FlatUI.initDefaultValues(getActivity());
        FlatUI.setDefaultTheme(FlatUI.DEEP);

        validator = new Validator(this);
        validator.setValidationListener(this);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

        return view;
    }

    @Override
    public void onValidationSucceeded() {
        BackgroundMail.newBuilder(getActivity())
                .withUsername("nl.calorie.calculator@gmail.com")
                .withPassword("nl123456")
                .withMailto("test@allfound.ru")
                .withSubject("Calorie Calculator User Feedback")
                .withBody(commentEditText.getText().toString())
                .withOnSuccessCallback(new BackgroundMail.OnSuccessCallback() {
                    @Override
                    public void onSuccess() {
                        //Toast.makeText(getActivity(), R.string.success, Toast.LENGTH_SHORT).show();
                        commentEditText.setText("");
                    }
                })
                .withOnFailCallback(new BackgroundMail.OnFailCallback() {
                    @Override
                    public void onFail() {
                        //Toast.makeText(getActivity(), R.string.failed, Toast.LENGTH_SHORT).show();
                    }
                })
                .withProcessVisibility(true)
                .send();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
