package de.android.caloriecalculator.fragment.Registration;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.DecimalMax;
import com.mobsandgeeks.saripaar.annotation.DecimalMin;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.android.caloriecalculator.R;
import de.android.caloriecalculator.activity.MainActivity;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.util.Calculation;
import de.android.caloriecalculator.util.Utils;

/*
 * RegistrationSecondStepFragment.java    v.1.1 29.06.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class RegistrationSecondStepFragment extends Fragment implements Validator.ValidationListener  {
    @NotEmpty @DecimalMin(value = 10) @DecimalMax(value = 35)
    @Bind(R.id.etIndex) EditText etIndex;
    @NotEmpty @DecimalMin(value = 130) @DecimalMax(value = 250)
    @Bind(R.id.etHeight) EditText etHeight;
    @NotEmpty @DecimalMin(value = 30) @DecimalMax(value = 200)
    @Bind(R.id.etWeight) EditText etWeight;
    @NotEmpty @DecimalMin(value = 16) @DecimalMax(value = 150)
    @Bind(R.id.etAge) EditText etAge;
    @NotEmpty @DecimalMin(value = 45) @DecimalMax(value = 250)
    @Bind(R.id.etWaistSize) EditText etWaistSize;
    @Bind(R.id.rgActivity) RadioGroup rgActivity;

    private Validator validator;
    View view;

    public RegistrationSecondStepFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_registartion_second_step, container, false);

        ButterKnife.bind(this, view);

        validator = new Validator(this);
        validator.setValidationListener(this);

        //load();
        return view;
    }

    @OnClick(R.id.finish_button)
    public void checkAndGo(){
        validator.validate();
    }

    private void load() {
        ClientCard currentManagerCard = Utils.getCurrentManagerCard(getActivity());
        etIndex.setText(String.valueOf(currentManagerCard.getIndexSolov()));
        etHeight.setText(String.valueOf(currentManagerCard.getHeight()));
        etWeight.setText(String.valueOf(currentManagerCard.getWeight()));
        etAge.setText(String.valueOf(currentManagerCard.getAge()));
        etWaistSize.setText(String.valueOf(currentManagerCard.getWaistSize()));
        int i = (int) currentManagerCard.getActivityСoeff();
        if (i >= 0) {
            ((RadioButton)rgActivity.getChildAt(i)).setChecked(true);
        }
    }

    private void save() {
        ClientCard currentManagerCard = Utils.getCurrentManagerCard(getActivity());

        currentManagerCard.setIndexSolov(Integer.valueOf(etIndex.getText().toString()));
        currentManagerCard.setHeight(Integer.valueOf(etHeight.getText().toString()));
        currentManagerCard.setWeight(Integer.valueOf(etWeight.getText().toString()));
        currentManagerCard.setAge(Integer.valueOf(etAge.getText().toString()));
        currentManagerCard.setWaistSize(Integer.valueOf(etWaistSize.getText().toString()));
        switch (rgActivity.getCheckedRadioButtonId()) {
            case R.id.rbActivityLow:
                currentManagerCard.setActivityСoeffLow();
                break;
            case R.id.rbActivitySmall:
                currentManagerCard.setActivityСoeffSmall();
                break;
            case R.id.rbActivityMiddle:
                currentManagerCard.setActivityСoeffMiddle();
                break;
            case R.id.rbActivityHigh:
                currentManagerCard.setActivityСoeffHigh();
                break;
        }

        Calculation calculation = new Calculation();
        calculation.calculate(currentManagerCard);
        Utils.setCurrentManagerCard(getActivity(), currentManagerCard);
        Utils.setInfoRegistrationPassed(getActivity(), true);
    }

    @Override
    public void onValidationSucceeded() {
        save();
        startActivity(new Intent(getActivity(), MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

}
