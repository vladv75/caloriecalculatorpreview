package de.android.caloriecalculator.fragment.EditClientProfile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.DecimalMax;
import com.mobsandgeeks.saripaar.annotation.DecimalMin;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.android.caloriecalculator.R;
import de.android.caloriecalculator.activity.EditClientProfileActivity;
import de.android.caloriecalculator.activity.MainActivity;
import de.android.caloriecalculator.activity.ResultCalculationActivity;
import de.android.caloriecalculator.activity.UserProfileActivity;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.dto.IClientCard;
import de.android.caloriecalculator.service.ClientDAO;
import de.android.caloriecalculator.util.Calculation;
import de.android.caloriecalculator.util.Utils;

import static java.lang.String.format;

/*
 * EditClientProfileSecondFragment.java    v.1.2 30.06.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class EditClientProfileSecondFragment extends Fragment
        implements IClientCard, Validator.ValidationListener {
    @NotEmpty @DecimalMin(value = 10) @DecimalMax(value = 35)
    @Bind(R.id.etIndex) EditText etIndex;
    @NotEmpty @DecimalMin(value = 130) @DecimalMax(value = 250)
    @Bind(R.id.etHeight) EditText etHeight;
    @NotEmpty @DecimalMin(value = 30) @DecimalMax(value = 200)
    @Bind(R.id.etWeight) EditText etWeight;
    @NotEmpty @DecimalMin(value = 16) @DecimalMax(value = 150)
    @Bind(R.id.etAge) EditText etAge;
    @NotEmpty @DecimalMin(value = 45) @DecimalMax(value = 250)
    @Bind(R.id.etWaistSize) EditText etWaistSize;
    @Bind(R.id.rgActivity) RadioGroup rgActivity;

    private Validator validator;
    private ClientCard clientCard;
    private EditClientProfileActivity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_editclientprofile_second, container, false);

        validator = new Validator(this);
        validator.setValidationListener(this);

        ButterKnife.bind(this, view);

        FloatingActionButton floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

        setupData(view);
        return view;
    }

    private void setupData(View view) {
        activity = (EditClientProfileActivity) getActivity();
        clientCard = activity.takeClientCard();

        if(activity.sourceActivity.equals("MainActivity")) {
            etHeight.setText("");
            etIndex.setText("");
            etWeight.setText("");
            etAge.setText("");
            etWaistSize.setText("");
        } else {
            etHeight.setText(String.valueOf(clientCard.getHeight()));
            etIndex.setText(String.valueOf(clientCard.getIndexSolov()));
            etWeight.setText(String.valueOf(clientCard.getWeight()));
            etAge.setText(String.valueOf(clientCard.getAge()));
            etWaistSize.setText(String.valueOf(clientCard.getWaistSize()));
        }

        float activityСoeff = clientCard.getActivityСoeff();
        if(activityСoeff == ACTIVITY_LOW) {
            RadioButton rbActivityLow = (RadioButton) view.findViewById(R.id.rbActivityLow);
            rbActivityLow.setChecked(true);
        } else if(activityСoeff == ACTIVITY_SMALL) {
            RadioButton rbActivitySmall = (RadioButton) view.findViewById(R.id.rbActivitySmall);
            rbActivitySmall.setChecked(true);
        } else if(activityСoeff == ACTIVITY_MIDDLE) {
            RadioButton rbActivityMiddle = (RadioButton) view.findViewById(R.id.rbActivityMiddle);
            rbActivityMiddle.setChecked(true);
        } else if(activityСoeff == ACTIVITY_HIGH) {
            RadioButton rbActivityHigh = (RadioButton) view.findViewById(R.id.rbActivityHigh);
            rbActivityHigh.setChecked(true);
        }
    }

    private void saveData() {
        try {
            clientCard.setIndexSolov(Integer.parseInt(etIndex.getText().toString()));
            clientCard.setHeight(Integer.parseInt(etHeight.getText().toString()));
            clientCard.setWeight(Integer.parseInt(etWeight.getText().toString()));
            clientCard.setAge(Integer.parseInt(etAge.getText().toString()));
            clientCard.setWaistSize(Integer.parseInt(etWaistSize.getText().toString()));
            switch (rgActivity.getCheckedRadioButtonId()) {
                case R.id.rbActivityLow:
                    clientCard.setActivityСoeffLow();
                    break;
                case R.id.rbActivitySmall:
                    clientCard.setActivityСoeffSmall();
                    break;
                case R.id.rbActivityMiddle:
                    clientCard.setActivityСoeffMiddle();
                    break;
                case R.id.rbActivityHigh:
                    clientCard.setActivityСoeffHigh();
                    break;
            }
        } catch (NumberFormatException nfe) {
            nfe.getMessage();
        }
    }

    protected void calculateData2DB() {
        Calculation calculation = new Calculation();
        calculation.calculate(clientCard);
        Intent intent;
        ClientDAO clientDAO = new ClientDAO(activity);

        switch(activity.sourceActivity) {
            case "MainActivity":
                clientDAO.save(clientCard);
                intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                break;
            case "ResultCalculationActivity":
                clientDAO.update(clientCard);
                intent = new Intent(getActivity(), ResultCalculationActivity.class);
                long id = clientCard.getId();
                intent.putExtra("id", id);
                startActivity(intent);
                break;
            case "UserProfileActivity":
                Utils.setCurrentManagerCard(getActivity(), clientCard);
                intent = new Intent(getActivity(), UserProfileActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {
        saveData();
        calculateData2DB();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
