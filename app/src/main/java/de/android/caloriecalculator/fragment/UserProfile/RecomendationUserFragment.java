package de.android.caloriecalculator.fragment.UserProfile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.android.caloriecalculator.R;
import de.android.caloriecalculator.activity.UserProfileActivity;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.util.Result;

/*
 * RecomendationFragment.java    v.1.0 01.04.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class RecomendationUserFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_recomendation, container, false);
        setupResultData(view);
        return view;
    }

    private void setupResultData(View view) {
        UserProfileActivity activity = (UserProfileActivity) getActivity();
        ClientCard managerCard = activity.takeManagerCard();
        Result result = new Result(managerCard, activity);

        TextView tvRecommendationMeal = (TextView) view.findViewById(R.id.tvRecommendationMeal);
        tvRecommendationMeal.setText(getString(R.string.preferred_foods_and_diet) + " \n"
                + result.fetchRecommendationMeal());

        TextView tvRecommendationNotMeal = (TextView) view.findViewById(R.id.tvRecommendationNotMeal);
        tvRecommendationNotMeal.setText(getString(R.string.it_is_desirable_to_limit_exclude) + " \n"
                + result.fetchRecommendationMeal());
    }

    public static Fragment newInstance() {
        return new RecomendationUserFragment();
    }

}