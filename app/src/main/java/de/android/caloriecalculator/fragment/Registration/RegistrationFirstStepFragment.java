package de.android.caloriecalculator.fragment.Registration;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.android.caloriecalculator.R;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.util.Utils;

/*
 * RegistrationFirstStepFragment.java    v.1.1 29.06.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class RegistrationFirstStepFragment extends Fragment implements Validator.ValidationListener  {
    @NotEmpty @Bind(R.id.ETFirstName) EditText ETFirstName;
    @NotEmpty @Bind(R.id.ETLastName) EditText ETLastName;
    @NotEmpty @Email @Bind(R.id.ETEmail) EditText ETEmail;
    @NotEmpty @Bind(R.id.ETPhone) EditText etTelephoneNumber;
    @NotEmpty @Bind(R.id.ETGender) EditText ETGender;

    private CharSequence[] items;

    private Validator validator;

    public RegistrationFirstStepFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_registartion_first_step, container, false);
        ButterKnife.bind(this, view);
        items = new CharSequence[]{getString(R.string.male), getString(R.string.female)};
        validator = new Validator(this);
        validator.setValidationListener(this);
        //load();

        ETGender.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length()>0){
                    ETGender.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        return view;
    }

    @OnClick(R.id.ETGender) void showAlertWithSelectGender(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.gender));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                ETGender.setText(items[item]);
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void load() {
        ClientCard currentManagerCard = Utils.getCurrentManagerCard(getActivity());
        ETFirstName.setText(currentManagerCard.getFirstName());
        ETLastName.setText(currentManagerCard.getLastName());
        ETEmail.setText(currentManagerCard.getEMail());
        etTelephoneNumber.setText(currentManagerCard.getTelNum());
    }

    private void save() {
        ClientCard currentManagerCard = new ClientCard(
                getActivity(),
                ETFirstName.getText().toString(),
                ETLastName.getText().toString(),
                0,
                ETEmail.getText().toString(),
                etTelephoneNumber.getText().toString(),
                0,
                0,
                0,
                0,
                0);
        Utils.setCurrentManagerCard(getActivity(), currentManagerCard);
    }

    @OnClick(R.id.next_button)
    public void checkAndGo(){
        validator.validate();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void goToNextStep(){
        save();
        RegistrationSecondStepFragment fragmentUsersList = new RegistrationSecondStepFragment();
        FragmentManager fragmentManager = getActivity().getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.enter_anim, R.animator.exit_anim);
        fragmentTransaction.replace(R.id.fragment_container, fragmentUsersList);
        fragmentTransaction.commit();
    }

    @Override
    public void onValidationSucceeded() {
        goToNextStep();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
