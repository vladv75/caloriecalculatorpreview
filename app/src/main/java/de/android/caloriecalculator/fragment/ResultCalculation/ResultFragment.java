package de.android.caloriecalculator.fragment.ResultCalculation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.android.caloriecalculator.R;
import de.android.caloriecalculator.activity.ResultCalculationActivity;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.util.Result;

/*
 * ResultFragment.java    v.1.0 01.04.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class ResultFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_result, container, false);
        setupResultData(view);
        return view;
    }

    private void setupResultData(View view) {
        ResultCalculationActivity activity = (ResultCalculationActivity) getActivity();
        ClientCard clientCard = activity.takeClientCard();
        Result result = new Result(clientCard, activity);

        TextView tvSomatoType = (TextView) view.findViewById(R.id.tvSomatoType);
        tvSomatoType.setText(result.fetchSomatoType());

        TextView tvRisk = (TextView) view.findViewById(R.id.tvRisk);
        tvRisk.setText(result.fetchIMTrisk());

        TextView tvIMT = (TextView) view.findViewById(R.id.tvIMT);
        tvIMT.setText(clientCard.getDataIMT() + " - " + result.fetchIMTclassification());

        TextView tvWeight = (TextView) view.findViewById(R.id.tvWeight);
        tvWeight.setText(clientCard.getWeightDown() + " / " + clientCard.getWeightUp());

        TextView tvBM = (TextView) view.findViewById(R.id.tvBM);
        tvBM.setText(String.format("%s", clientCard.getDataBM()));

        TextView tvOO = (TextView) view.findViewById(R.id.tvOO);
        tvOO.setText(String.format("%s", String.format("%.2f", clientCard.getDataOO())));

        TextView tvMSG = (TextView) view.findViewById(R.id.tvMSG);
        tvMSG.setText(String.format("%s / %s",
                String.format("%.2f", clientCard.getDataMSGdown()),
                String.format("%.2f", clientCard.getDataMSGup())));

        TextView tvBGU = (TextView) view.findViewById(R.id.tvBGU);
        tvBGU.setText(getString(R.string.balance_in_normal));

        TextView tvProteinsNorm = (TextView) view.findViewById(R.id.tvProteinsNorm);
        tvProteinsNorm.setText(String.format("%s / %s",
                String.format("%.2f", clientCard.getProteinsNormCal()),
                String.format("%.2f", clientCard.getProteinsNormGram())));

        TextView tvFatsNorm = (TextView) view.findViewById(R.id.tvFatsNorm);
        tvFatsNorm.setText(String.format("%s / %s",
                String.format("%.2f", clientCard.getFatsNormCal()),
                String.format("%.2f", clientCard.getFatsNormGram())));

        TextView tvCarbsNorm = (TextView) view.findViewById(R.id.tvCarbsNorm);
        tvCarbsNorm.setText(String.format("%s / %s",
                String.format("%.2f", clientCard.getCarbsNormCal()),
                String.format("%.2f", clientCard.getCarbsNormGram())));

        TextView tvBGUdown = (TextView) view.findViewById(R.id.tvBGUdown);
        tvBGUdown.setText(getString(R.string.balance_by_reducing_the_weight));

        TextView tvProteinsDown = (TextView) view.findViewById(R.id.tvProteinsDown);
        tvProteinsDown.setText(String.format("%s / %s",
                String.format("%.2f", clientCard.getProteinsDownCal()),
                String.format("%.2f", clientCard.getProteinsDownGram())));

        TextView tvFatsDown = (TextView) view.findViewById(R.id.tvFatsDown);
        tvFatsDown.setText(String.format("%s / %s",
                String.format("%.2f", clientCard.getFatsDownCal()),
                String.format("%.2f", clientCard.getFatsDownGram())));

        TextView tvCarbsDown = (TextView) view.findViewById(R.id.tvCarbsDown);
        tvCarbsDown.setText(String.format("%s / %s",
                String.format("%.2f", clientCard.getCarbsDownCal()),
                String.format("%.2f", clientCard.getCarbsDownGram())));
    }

    public static Fragment newInstance() {
        return new ResultFragment();
    }
}
