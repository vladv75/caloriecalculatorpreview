package de.android.caloriecalculator.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rohit.recycleritemclicksupport.RecyclerItemClickSupport;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.android.caloriecalculator.R;
import de.android.caloriecalculator.activity.EditClientProfileActivity;
import de.android.caloriecalculator.activity.ResultCalculationActivity;
import de.android.caloriecalculator.adapter.ClientCardRecyclerAdapter;
import de.android.caloriecalculator.dto.ClientCard;
import de.android.caloriecalculator.service.ClientDAO;

/*
 * ClientsFragment.java    v.1.0 11.03.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public class ClientsFragment extends Fragment {

    private ArrayList<ClientCard> clientCards = new ArrayList<>();
    @Bind(R.id.recyclerview) RecyclerView recyclerView;
    private ClientDAO clientDAO;
    private static ClientCard clientCard;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        clientDAO = new ClientDAO(getActivity());
        //setTestData();
        AsyncLoadClientCardsFromBD asyncLoadClientCardsFromBD = new AsyncLoadClientCardsFromBD();
        asyncLoadClientCardsFromBD.execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_clients, container, false);

        ButterKnife.bind(this, view);
        RecyclerItemClickSupport.addTo(recyclerView).setOnItemClickListener(new RecyclerItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                clientCard = clientCards.get(position);
                Log.d("LOG", clientCard + "");
                Log.d("LOG", String.valueOf(clientCard.getId()));
                Intent intent = new Intent(getContext(), ResultCalculationActivity.class);
                long id = clientCard.getId();
                intent.putExtra("id", id);
                intent.putExtra("sourceActivity", "MainActivityEdit");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), EditClientProfileActivity.class);
                intent.putExtra("id", 0);
                intent.putExtra("sourceActivity", "MainActivity");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        return view;
    }

    public static ClientCard getClientCard() {
        return clientCard;
    }

    public static void setClientCard(ClientCard clientCard) {
        ClientsFragment.clientCard = clientCard;
    }

    private void setupRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        setContactsAdapter(clientCards);
    }

    private void setContactsAdapter(List<ClientCard> clientCards) {
        recyclerView.setAdapter(new ClientCardRecyclerAdapter(getActivity(), clientCards));
    }

    //TEST function
    private void setTestData() {
        ClientCard clientCard = new ClientCard(getActivity());
        clientCard.setFirstName("Иван");
        clientCard.setLastName("Петров");
        clientCard.seteMail("Ivan.Petrov@gmail.com");
        clientCard.setTelNum("+79237651567");

        //clientCards = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            //clientCards.add(clientCard);
            clientDAO.save(clientCard);
        }
    }

    private class AsyncLoadClientCardsFromBD extends AsyncTask<Void, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = "";
            //загружаем данные из БД
            clientDAO.fetchAll(clientCards);
            return result;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            setupRecyclerView();
//            Toast.makeText(getActivity(),
//                    getResources().getText(R.string.textLoadAllData), Toast.LENGTH_SHORT).show();
        }
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
