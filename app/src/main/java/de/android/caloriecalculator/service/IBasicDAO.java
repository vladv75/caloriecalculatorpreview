package de.android.caloriecalculator.service;

/*
 * DatabaseOpenHelper.java    v.1.0 18.03.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

import java.util.List;

public interface IBasicDAO<T> {
    long save(T object);
    T findById(long id);
    void fetchAll(List<T> object);
    void delete(long id);
    void deleteAll();
    void update(T object);
}
