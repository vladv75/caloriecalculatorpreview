package de.android.caloriecalculator.service;

/*
 * ClientDAO.java    v.1.0 09.03.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;

import java.util.List;

import de.android.caloriecalculator.dto.ClientCard;

public class ClientDAO extends ClientDBDAO implements IBasicDAO<ClientCard> {
    private Context context;

    public ClientDAO(Context context) {
        super(context);
        this.context = context;
    }

    public long save(ClientCard clientCard) {
        long rowID = -1;
        open();
        ContentValues contentValues = new ContentValues();
        putData2DB(contentValues, clientCard);
        try {
            rowID = database.insert(DatabaseOpenHelper.TABLE_CLIENTS, null, contentValues);
        } catch (SQLiteConstraintException ex) {
            System.out.print(ex.toString());
        }
        clientCard.setId(rowID);
        close();
        return rowID;
    }

    @Override
    public ClientCard findById(long id) {
        ClientCard clientCard = null;
        String sql = "SELECT * FROM " + DatabaseOpenHelper.TABLE_CLIENTS
                + " WHERE " + DatabaseOpenHelper.ID_COLUMN + " = ?";

        open();
        Cursor cursor = database.rawQuery(sql, new String[] { id + "" });
        if (cursor.moveToNext()) {
            clientCard = new ClientCard(context);
            putData2ClientCard(clientCard, cursor);
        }
        cursor.close();
        close();
        return clientCard;
    }

    @Override
    public void fetchAll(List<ClientCard> clientCards) {
        open();
        Cursor cursor = getCursorForBD();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            //создаем новую карточку учета
            ClientCard clientCard = new ClientCard(context);
            putData2ClientCard(clientCard, cursor);
            clientCards.add(0, clientCard);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        close();
    }

    // updates an existing contact in the database
    public void update(ClientCard clientCard) {
        ContentValues contentValues = new ContentValues();
        putData2DB(contentValues, clientCard);
        open(); // open the database
        database.update(DatabaseOpenHelper.TABLE_CLIENTS, contentValues,
                DatabaseOpenHelper.ID_COLUMN + "=" + clientCard.getId(), null);
        close(); // close the database
    }

    public void delete(long id) {
        open();
        database.delete(DatabaseOpenHelper.TABLE_CLIENTS,
                DatabaseOpenHelper.ID_COLUMN + "=" + id, null);
        close();
    }

    public void deleteAll() {
        open();
        database.delete(DatabaseOpenHelper.TABLE_CLIENTS, null, null);
        close();
    }

    private void putData2ClientCard(ClientCard clientCard, Cursor cursor) {
        clientCard.setId(cursor.getLong(0));
        clientCard.setFirstName(cursor.getString(1));
        clientCard.setLastName(cursor.getString(2));
        clientCard.setGender(cursor.getInt(3));
        clientCard.seteMail(cursor.getString(4));
        clientCard.setTelNum(cursor.getString(5));
        clientCard.setIndexSolov(cursor.getInt(6));
        clientCard.setHeight(cursor.getInt(7));
        clientCard.setWeight(cursor.getInt(8));
        clientCard.setAge(cursor.getInt(9));
        clientCard.setWaistSize(cursor.getInt(10));
        clientCard.setActivityСoeff(cursor.getFloat(11));
        clientCard.setDataIMT(cursor.getFloat(12));
        clientCard.setWeightDown(cursor.getFloat(13));
        clientCard.setWeightUp(cursor.getFloat(14));
        clientCard.setDataBM(cursor.getFloat(15));
        clientCard.setDataOO(cursor.getFloat(16));
        clientCard.setDataMSGdown(cursor.getFloat(17));
        clientCard.setDataMSGup(cursor.getFloat(18));
        clientCard.setProteinsNormCal(cursor.getFloat(19));
        clientCard.setProteinsNormGram(cursor.getFloat(20));
        clientCard.setFatsNormCal(cursor.getFloat(21));
        clientCard.setFatsNormGram(cursor.getFloat(22));
        clientCard.setCarbsNormCal(cursor.getFloat(23));
        clientCard.setCarbsNormGram(cursor.getFloat(24));
        clientCard.setProteinsDownCal(cursor.getFloat(25));
        clientCard.setProteinsDownGram(cursor.getFloat(26));
        clientCard.setFatsDownCal(cursor.getFloat(27));
        clientCard.setFatsDownGram(cursor.getFloat(28));
        clientCard.setCarbsDownCal(cursor.getFloat(29));
        clientCard.setCarbsDownGram(cursor.getFloat(30));
        clientCard.setSomatoType(cursor.getInt(31));
        clientCard.setClasIMT(cursor.getInt(32));
        clientCard.setOptimalWaist(cursor.getInt(33));
    }

    private void putData2DB(ContentValues contentValues, ClientCard clientCard) {
        contentValues.put(DatabaseOpenHelper.FIRST_NAME, clientCard.getFirstName());
        contentValues.put(DatabaseOpenHelper.LAST_NAME, clientCard.getLastName());
        contentValues.put(DatabaseOpenHelper.GENDER, clientCard.getGender());
        contentValues.put(DatabaseOpenHelper.EMAIL, clientCard.getEMail());
        contentValues.put(DatabaseOpenHelper.TELNUM, clientCard.getTelNum());
        contentValues.put(DatabaseOpenHelper.INDEX_SOLOV, clientCard.getIndexSolov());
        contentValues.put(DatabaseOpenHelper.HEIGHT, clientCard.getHeight());
        contentValues.put(DatabaseOpenHelper.WEIGHT, clientCard.getWeight());
        contentValues.put(DatabaseOpenHelper.AGE, clientCard.getAge());
        contentValues.put(DatabaseOpenHelper.WAIST_SIZE, clientCard.getWaistSize());
        contentValues.put(DatabaseOpenHelper.ACTIVITY_COEFF, clientCard.getActivityСoeff());
        contentValues.put(DatabaseOpenHelper.DATA_IMT, clientCard.getDataIMT());
        contentValues.put(DatabaseOpenHelper.WEIGHT_DOWN, clientCard.getWeightDown());
        contentValues.put(DatabaseOpenHelper.WEIGHT_UP, clientCard.getWeightUp());
        contentValues.put(DatabaseOpenHelper.DATA_BM, clientCard.getDataBM());
        contentValues.put(DatabaseOpenHelper.DATA_OO, clientCard.getDataOO());
        contentValues.put(DatabaseOpenHelper.DATA_MSG_DOWN, clientCard.getDataMSGdown());
        contentValues.put(DatabaseOpenHelper.DATA_MSG_UP, clientCard.getDataMSGup());
        contentValues.put(DatabaseOpenHelper.PROTEINS_NORM_CAL, clientCard.getProteinsNormCal());
        contentValues.put(DatabaseOpenHelper.PROTEINS_NORM_GRAM, clientCard.getProteinsNormGram());
        contentValues.put(DatabaseOpenHelper.FATS_NORM_CAL, clientCard.getFatsNormCal());
        contentValues.put(DatabaseOpenHelper.FATS_NORM_GRAM, clientCard.getFatsNormGram());
        contentValues.put(DatabaseOpenHelper.CARBS_NORM_CAL, clientCard.getCarbsNormCal());
        contentValues.put(DatabaseOpenHelper.CARBS_NORM_GRAM, clientCard.getCarbsNormGram());
        contentValues.put(DatabaseOpenHelper.PROTEINS_DOWN_CAL, clientCard.getProteinsDownCal());
        contentValues.put(DatabaseOpenHelper.PROTEINS_DOWN_GRAM, clientCard.getProteinsDownGram());
        contentValues.put(DatabaseOpenHelper.FATS_DOWN_CAL, clientCard.getFatsDownCal());
        contentValues.put(DatabaseOpenHelper.FATS_DOWN_GRAM, clientCard.getFatsDownGram());
        contentValues.put(DatabaseOpenHelper.CARBS_DOWN_CAL, clientCard.getCarbsDownCal());
        contentValues.put(DatabaseOpenHelper.CARBS_DOWN_GRAM, clientCard.getCarbsDownGram());
        contentValues.put(DatabaseOpenHelper.SOMATO_TYPE, clientCard.getSomatoType());
        contentValues.put(DatabaseOpenHelper.CLAS_IMT, clientCard.getClasIMT());
        contentValues.put(DatabaseOpenHelper.OPTIMAL_WAIST, clientCard.getOptimalWaist());
    }
}
