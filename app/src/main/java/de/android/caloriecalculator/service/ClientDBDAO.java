package de.android.caloriecalculator.service;

/*
 * ClientDBDAO.java    v.1.0 18.03.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ClientDBDAO {
    protected SQLiteDatabase database;
    private DatabaseOpenHelper databaseOpenHelper;
    private Context context;

    public ClientDBDAO(Context context) {
        databaseOpenHelper = DatabaseOpenHelper.getHelper(context);
        this.context = context;
    }

    public void open() throws SQLException {
        if (databaseOpenHelper == null)
            databaseOpenHelper = DatabaseOpenHelper.getHelper(context);
        database = databaseOpenHelper.getWritableDatabase();
    }

    public void close() {
        if (database != null)
            database.close();
    }

    public Cursor getCursorForBD() {
        return database.query(DatabaseOpenHelper.TABLE_CLIENTS, null, null, null, null, null, null);
    }
}
