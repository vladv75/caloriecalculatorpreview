package de.android.caloriecalculator.service;

/*
 * DatabaseOpenHelper.java    v.1.0 18.03.2016
 *
 * Copyright (c) 2016 Vladislav Laptev,
 * All rights reserved. Used by permission.
 */

public interface IClientSchema {
    // If you change the database schema, you must increment the database version.
    int DATABASE_VERSION = 3;
    String DATABASE_NAME = "CalorieCalculator";
    String TABLE_CLIENTS = "clients";

    String ID_COLUMN = "id";
    String FIRST_NAME = "firstName";
    String LAST_NAME = "lastName";
    String GENDER = "gender";
    String EMAIL = "eMail";
    String TELNUM = "telNum";
    String INDEX_SOLOV = "indexSolov";
    String HEIGHT = "height";
    String WEIGHT = "weight";
    String AGE = "age";
    String WAIST_SIZE = "waistSize";
    String ACTIVITY_COEFF = "activityСoeff";
    String DATA_IMT = "dataIMT";
    String WEIGHT_DOWN = "weightDown";
    String WEIGHT_UP = "weightUp";
    String DATA_BM = "dataBM";
    String DATA_OO = "dataOO";
    String DATA_MSG_DOWN = "dataMSGdown";
    String DATA_MSG_UP = "dataMSGup";
    String PROTEINS_NORM_CAL = "proteinsNormCal";
    String PROTEINS_NORM_GRAM = "proteinsNormGram";
    String FATS_NORM_CAL = "fatsNormCal";
    String FATS_NORM_GRAM = "fatsNormGram";
    String CARBS_NORM_CAL = "carbsNormCal";
    String CARBS_NORM_GRAM = "carbsNormGram";
    String PROTEINS_DOWN_CAL = "proteinsDownCal";
    String PROTEINS_DOWN_GRAM = "proteinsDownGram";
    String FATS_DOWN_CAL = "fatsDownCal";
    String FATS_DOWN_GRAM = "fatsDownGram";
    String CARBS_DOWN_CAL = "carbsDownCal";
    String CARBS_DOWN_GRAM = "carbsDownGram";
    String SOMATO_TYPE = "somatoType";
    String CLAS_IMT = "clasIMT";
    String OPTIMAL_WAIST = "optimalWaist";

    String CREATE_CLIENT_TABLE = "CREATE TABLE "
            + TABLE_CLIENTS + "(" + ID_COLUMN + " integer primary key autoincrement,"
            + FIRST_NAME + " TEXT, "
            + LAST_NAME + " TEXT, "
            + GENDER + " INTEGER, "
            + EMAIL + " TEXT, "
            + TELNUM + " TEXT, "
            + INDEX_SOLOV + " INTEGER, "
            + HEIGHT + " INTEGER, "
            + WEIGHT + " INTEGER, "
            + AGE + " INTEGER, "
            + WAIST_SIZE + " INTEGER, "
            + ACTIVITY_COEFF + " REAL, "
            + DATA_IMT + " REAL, "
            + WEIGHT_DOWN + " REAL, "
            + WEIGHT_UP + " REAL, "
            + DATA_BM + " REAL, "
            + DATA_OO + " REAL, "
            + DATA_MSG_DOWN + " REAL, "
            + DATA_MSG_UP + " REAL, "
            + PROTEINS_NORM_CAL + " REAL, "
            + PROTEINS_NORM_GRAM + " REAL, "
            + FATS_NORM_CAL + " REAL, "
            + FATS_NORM_GRAM + " REAL, "
            + CARBS_NORM_CAL + " REAL, "
            + CARBS_NORM_GRAM + " REAL, "
            + PROTEINS_DOWN_CAL + " REAL, "
            + PROTEINS_DOWN_GRAM + " REAL, "
            + FATS_DOWN_CAL + " REAL, "
            + FATS_DOWN_GRAM + " REAL, "
            + CARBS_DOWN_CAL + " REAL, "
            + CARBS_DOWN_GRAM + " REAL, "
            + SOMATO_TYPE + " INTEGER, "
            + CLAS_IMT + " INTEGER, "
            + OPTIMAL_WAIST + " INTEGER "
            + ");";
}
